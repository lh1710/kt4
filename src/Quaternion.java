
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Quaternions. Basic operations. */
public class Quaternion {

   private static final double constant = 0.0001;

   private double realPart;
   private double i;
   private double j;
   private double k;

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      this.realPart = a;
      this.i = b;
      this.j = c;
      this.k = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return this.realPart;
   }

   /** Imaginary part i of the quaternion.
    * @return imaginary part i
    */
   public double getIpart() {
      return this.i;
   }

   /** Imaginary part j of the quaternion.
    * @return imaginary part j
    */
   public double getJpart() {
      return this.j;
   }

   /** Imaginary part k of the quaternion.
    * @return imaginary part k
    */
   public double getKpart() {
      return this.k;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion:
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      String full = "";

      if (this.realPart > 0) {
         full += this.realPart;
      }
      else if (this.realPart < 0){
          full += this.realPart;
      }

      if (this.i > 0) {
         full += "+" + this.i+ "i";
      }
      else if (this.i < 0){
         full += this.i + "i";
      }

      if (this.j > 0) {
         full += "+" + this.j + "j";
      }
      else if (this.j < 0){
         full += this.j + "j";
      }

      if (this.k > 0) {
         full += "+" + this.k + "k";
      }
      else if (this.k < 0){
         full += this.k + "k";
      }
      return full;
   }

   public static Quaternion valueOf (String s) throws IllegalArgumentException{
      Pattern p = Pattern.compile("(\\+||-)\\d+.0+(i|j|k|)");
      ArrayList<String> listi = new ArrayList<>();
      Matcher matcher = p.matcher(s);

      Pattern pattern = Pattern.compile("i");
      Matcher m1 = pattern.matcher(s);
      int counti = 0;
      while (m1.find()) {
         counti++;
      }
      if (counti > 1) {
         throw new RuntimeException("Too many i's!");
      }

      Pattern p2 = Pattern.compile("j");
      Matcher m2 = p2.matcher(s);
      int countj = 0;
      while (m2.find()) {
         countj++;
      }
      if (countj > 1) {
         throw new RuntimeException("Too many j's!");
      }

      Pattern p3 = Pattern.compile("k");
      Matcher m3 = p3.matcher(s);
      int countk = 0;
      while (m3.find()) {
         countk++;
      }
      if (countk > 1) {
         throw new RuntimeException("Too many k's!");
      }


      while (matcher.find()) {
         listi.add(matcher.group());
      }
      if (listi.size() == 4) {
         if (!String.valueOf(listi.get(1).charAt(listi.get(1).length() - 1)).equals("i")) {
            throw new RuntimeException("Wrong character!");
         }
         if (!String.valueOf(listi.get(2).charAt(listi.get(2).length() - 1)).equals("j")) {
            throw new RuntimeException("Wrong character!");
         }
         if (!String.valueOf(listi.get(3).charAt(listi.get(3).length() - 1)).equals("k")) {
            throw new RuntimeException("Wrong character!");
         }
      }

      float real = 0.0f;
      float i = 0.0f;
      float j = 0.0f;
      float k = 0.0f;

      for (String x: listi) {
         String s1 = String.valueOf(x.charAt(x.length() - 1));
         float k1 = Float.parseFloat(x.substring(0, x.length() - 1));
         switch (s1) {
            case "i":
               i = k1;
               break;
            case "j":
               j = k1;
               break;
            case "k":
               k = k1;
               break;
            default:
               real = k1;
               break;
         }
      }
      return new Quaternion(real, i, j, k);
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(realPart, i, j, k);
   }

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      return findDifference(0, this.realPart) && findDifference(0, this.i) && findDifference(0, this.j) && findDifference(0, this.k);
   }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      return new Quaternion(this.realPart, -this.i, -this.j, -this.k);
   }

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
      return new Quaternion(-this.realPart, -this.i, -this.j, -this.k);
   }

   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
      return new Quaternion(this.realPart+q.realPart, this.i+q.i, this.j+q.j, this.k+q.k);
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
      double a = q.realPart;
      double b = q.i;
      double c = q.j;
      double d = q.k;

      double one = this.realPart;
      double two = this.i;
      double three = this.j;
      double four = this.k;

      double realPart = one * a - two * b - three * c - four * d;
      double i = one * b + two * a + three * d - four * c;
      double j = one * c - two * d + three * a + four * b;
      double k = one * d + two * c - three * b + four * a;

      return new Quaternion(realPart, i, j, k);

   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      return new Quaternion(this.realPart*r, this.i*r, this.j*r, this.k*r);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() throws RuntimeException{
      if ((findDifference(this.realPart, 0) && findDifference(this.i, 0)) && findDifference(this.j, 0) && findDifference(this.k, 0)) {
         throw new RuntimeException("Cant divide by 0!");
      }
      double x = this.realPart * this.realPart + this.i * this.i + this.j * this.j + this.k * this.k;
      return new Quaternion(this.realPart/x, (-this.i)/x, (-this.j)/x, (-this.k)/x);
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      return this.plus(q.opposite());
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) throws RuntimeException{
      if ((findDifference(q.realPart, 0) || findDifference(q.i, 0)) || findDifference(q.j, 0) || findDifference(q.k, 0)) {
         throw new RuntimeException("Cant divide by 0!");
      }
      return this.times(q.inverse());
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) throws RuntimeException{
      if ((findDifference(q.realPart, 0) || findDifference(q.i, 0)) || findDifference(q.j, 0) || findDifference(q.k, 0)) {
         throw new RuntimeException("Cant divide by 0!");
      }
      return q.inverse().times(this);
   }
   
   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
      if(qo instanceof Quaternion) {
         return findDifference(this.realPart, ((Quaternion) qo).getRpart()) && findDifference(this.i, ((Quaternion) qo).getIpart()) 
                 && findDifference(this.j, ((Quaternion) qo).getJpart()) && findDifference(this.k, ((Quaternion) qo).getKpart());
      }
      return false;
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
      return this.times(q.conjugate()).plus(q.times(this.conjugate())).times(0.5);
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Arrays.hashCode(new double[] {this.realPart, this.i, this.j, this.k});

   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(this.realPart*this.realPart+this.i*this.i+this.j*this.j+this.k*this.k);

   }

   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: " 
         + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));
   }

   public static boolean findDifference(double a, double b){
      return Math.abs(a - b) < constant;
   }
}
// end of file
